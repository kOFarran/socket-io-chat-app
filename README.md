# Socket io Chat App

React web app chat system using socket.io.

The system allows users to set up and join chat rooms. The names of chat room members are displayed on the left of the screen for larger devices. The UI is responsive to different screen sizes. Clients recieve web notifications from the admin (server) when users join/leave the chat room they are in.

# How to run app locally

There are two directories (client and server). 
Open a terminal in each of the directories.
Install the dependencies in both directories with 'npm install'.

Boot up the express server, in the server directory, first with the command 'npm start'.
Then in the second terminal for the client directory, run the same command 'npm start'.
This should open a browser page on localhost:3000.