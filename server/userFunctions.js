// array containing objects with each user's info
const users = [];

const addUser = ({id, name, room}) => {

    //trimming and converting the usernames and rooms to lower case to avoid any syntax problems
    name = name.trim()
    room = room.trim()

    // check if the username already exists in the array
    const exists = users.find((user) => user.name === name && user.room === room);
    if(exists){
        return {error: "Username is taken"};
    }

    const user = {id, name, room};
    users.push(user);
    return{user};
}

const removeUser = (id) => {
    // get the index of the user in the array
    const userIndex = users.findIndex((user) => user.id === id);

    // check to make sure the array is not empty and the user exists, then remove
    if(userIndex !== -1){
       return users.splice(userIndex, 1)[0];
    }
    
}

const getUser = (id) => 
    // find a user by their socket id
    users.find((user) => user.id === id);


const getUsersInRoom = (room) => 
    // get an array of the users in a specific room
    users.filter((user) => user.room === room);


module.exports = {addUser, removeUser, getUser, getUsersInRoom }