const express = require('express');
const http = require('http');
const socketio = require('socket.io');
const router = require('./router');
// importing functions defined in the userFunctions file
const { addUser, removeUser, getUser, getUsersInRoom } = require('./userFunctions');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const PORT = process.env.PORT || 5000

app.use(router);

server.listen(PORT, () => console.log(`server has started on port ${PORT}`));

// io is called when a new connection is made
io.on('connection', (socket) => {
    console.log("New Connection");

    // socket listener for when a new user is added to a chatroom
    socket.on("userAdded", ({name, room}, callback) => {
        console.log(`${name} added to room ${room}`);

        // calling function defined in the userFunctions file
        const {error, user} = addUser({id: socket.id, name, room});

        // error handling if a username is already taken in a chat room
        if(error){
            return callback(error);
        }

        // emiting two messages here, one is broadcast so that every user in the room besides the new user can see it
        socket.broadcast.to(user.room).emit("adminMessage", {user: 'admin', message: `A wild ${user.name} appears!`});

        // second message is only visible to the newly joined user
        socket.emit("adminMessage", {user: 'admin', message: `${user.name}, welcome to chat room: ${user.room} ` });

        // subscribe the socket to the room 
        socket.join(user.room);
        io.to(user.room).emit('roomData', {room: user.room, users: getUsersInRoom(user.room)});

    });

    // listens for when users send messages and emits the message to all users in the same room
    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id);
        // propagate the message to all the users in the room
        io.to(user.room).emit('message', {user: user.name, message: message});
        callback();
    });

    //listening for a disconnection from this socket
    socket.on('disconnect', () => {
        console.log("User Disconnected :(");

        // when user disconnects, remove them from the users array 
        const user = removeUser(socket.id);
        if (user){
            // emit an admin message so each member still in the room recieves a notification
            io.to(user.room).emit('adminMessage', {user: 'admin', message: `${user.name} has left the chat.`});
            // emit the usernames of those still in the room
            io.to(user.room).emit('roomData', {room: user.room, users: getUsersInRoom(user.room)});
        }
        
    });

});