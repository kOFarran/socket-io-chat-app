import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Start from './components/Start/Start';
import Chat from './components/Chat/Chat';
import ReactNotifications from 'react-notifications-component';

// main component that handles the routing between the other components
const App = () => (

    <Router>
        {/* ReactNotification component allows for web notifications in any desired component */}
        <ReactNotifications />
        <Route path="/" exact component={Start} />
        <Route path="/chat" component={Chat} />
    </Router>
);

export default App;