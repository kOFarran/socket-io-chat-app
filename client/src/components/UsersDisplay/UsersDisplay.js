import React from 'react';

import './UsersDisplay.css';

// component for displaying the users that are currently in the chat room
const UsersDisplay = ({ users }) => {
    return(
        users
            ? (
            <div className="innerTextContainer">
                <h3 className="label">Users In Chat</h3>
                <div className="bottomTextContainer" >
                    {users.map(({name}) => (
                    <div className="member-card mt-3">
                        <h6 key={name} className="member-name">{name}</h6>
                    </div>
                    ))}
                </div>
            </div>
            )
            : null
          
    );

}
export default UsersDisplay;