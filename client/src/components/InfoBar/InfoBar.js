import React from 'react';
import closeIcon from '../../icons/closeIcon2.png';
import './InfoBar.css';

// component for the info bar displaying the name of the chat room
const InfoBar = ( {room} ) => (
    <div className="infoBar">
        <div className="leftInnerContainer">
            <h3>{room}</h3>
        </div>
        <div className="rightInnerContainer">
            <a className="closeLink" href="/">
                <img className="close" src={closeIcon} alt="close image" />
            </a>
        </div>
    </div>
)

export default InfoBar;