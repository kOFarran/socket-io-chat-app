import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Start.css'

//building the components as functions and making use of react hooks
const Start = () => {

    // react hooks for creating and setting state variables
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    return(
        <div className="startOuterContainer">
            <div className="startContainer">
                <h1 className="header">Join A Room</h1>
                <div>
                <input className="startInput" placeholder="Name" onChange={(event) => {setName(event.target.value)}} />
                </div>
                <div>
                <input className="startInput" placeholder="Room" onChange={(event) => {setRoom(event.target.value)}} />
                </div>
                <Link onClick={event => !name || !room ? event.preventDefault() : null} to={`/chat?name=${name}&room=${room}`}>
                    <button className="button mt-20" type="submit">Connect</button>
                </Link>
            </div>

        </div>
    )
}

export default Start;