import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';
import './Chat.css';
import InfoBar from '../InfoBar/InfoBar';
import Input from '../Input/Input';
import Messages from '../Messages/Messages';
import UsersDisplay from '../UsersDisplay/UsersDisplay';
import { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';

// initialising the socket variable
var socket;

// building the components as functions and making use of react hooks
const Chat = ({location}) => {

    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [users, setUsers] = useState('');
    const ENDPOINT = 'localhost:5000';

    // userEffect works similarly to ComponentDidMount and runs each time variables in array change
    useEffect(() => {

        // using the query string to identify room location for socket
        const {name, room} = queryString.parse(location.search);
        console.log(name, room);

        socket = io(ENDPOINT);

        setName(name);
        setRoom(room);

        // notify users in the room that this new user has been added
        socket.emit('userAdded', {name, room}, (error) =>{
            // error is called if the username is aleady taken in the room. User is then redirected to the Start component to choose a new username
            if(error){
                alert(error);
                window.location.href="/";
            }
        });

        // socket listener for admin messages to be displayed as web notifications on the top-right of the screen
        socket.on("adminMessage", (message) =>{
            var notification = message.message;
                store.addNotification({
                    title: 'Admin Message',
                    message: notification,
                    type: 'info', 
                    container: 'top-right',                
                    animationIn: ["animated", "fadeIn"],    
                    animationOut: ["animated", "fadeOut"],   
                    // duration of admin notifications set to 10 seconds before fading out
                    dismiss: {
                      duration: 10000,
                      pauseOnHover: true,
                      click: true,
                      showIcon: true
                    }
                  })
          });

          // socket listener for incoming messages
          socket.on('message', (message) => {
            setMessages(messages =>[... messages, message]);
        });

        // socket listener for incoming data on the room members
        socket.on("roomData", ({ users }) => {
            setUsers(users);
          });

        // return called when component unmounts
        return () => {
            // emit the socket disconnect and set the socket to off
            socket.emit('diconnect');
            socket.off();
        }
    }, [ENDPOINT, location.search]);

    //function for sending messages
    const sendMessage = (event) => {
        // prevent the default function of the submit 
        event.preventDefault();
        if(message){
            socket.emit('sendMessage', message, () => setMessage(''));
        }
    }


    return(
        <div className="outerContainer">   
            <UsersDisplay users={users} /> 
            <div className="container">
                <InfoBar room={room} />
                <Messages messages={messages} name={name} />
                <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
            </div>
        </div>
        
    )
}

export default Chat;