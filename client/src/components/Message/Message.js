import React from 'react';
import './Message.css';
import ReactEmoji from 'react-emoji';

// message component for the chat bubbles containing the users' messages
const Message = ( {message: {user, message}, name} ) => {
    let isSentByCurrentUser = false;

    const trimmedName = name.trim();

    if( user === trimmedName){
        isSentByCurrentUser = true;
    }
    // if the message is sent by the current user, display the chat bubble on the right side of the chat box with a green background
    // else the message is from another user, display the chat bubble on the left side of the screen with a grey background
    return(
        isSentByCurrentUser
        ? (
            <div className="messageContainer justifyEnd"> 
                <p className="sentText pr-10">{trimmedName}</p>
                <div className="messageBox backgroundGreen">
                    <p className="messageText colorLight">{ReactEmoji.emojify(message)}</p>
                </div>
            </div>
        )
        : (
            <div className="messageContainer justifyStart"> 
                
                <div className="messageBox backgroundLight">
                    <p className="messageText colorDark">{ReactEmoji.emojify(message)}</p>
                </div>
                <p className="sentText pl-10">{user}</p>
            </div>

        )
    )
}


export default Message;